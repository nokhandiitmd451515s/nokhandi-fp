/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.fp.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import org.eclipse.persistence.annotations.CascadeOnDelete;

/**
 *
 * @author nokhandiar
 *
 * This bean is used to store all the threads of our forum. Each thread knows:
 * Who created it Subject/Description Any comments made, attached to the thread
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "ForumThread.findAll",
            query = "select u from ForumThread u"),
    @NamedQuery(name = "ForumThread.exists",
            query = "select u from ForumThread u where u.id = :id"),
    @NamedQuery(name = "ForumThread.findById",
            query = "select u from ForumThread u where u.id = :id"),})
public class ForumThread implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private String subject;
    private String description;
    private Users creator;
    private boolean adminOnly;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "thread")
    @JoinColumn(name = "Admins")
    @CascadeOnDelete
    private List<Admins> admins;

    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "thread", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "Comments",
            joinColumns = { 
                @JoinColumn(name = "comments",
                        referencedColumnName = "id")
            })
    @CascadeOnDelete
    private List<ThreadComment> comments;

    public ForumThread() {
        this.adminOnly = false;
    }

    public ForumThread(String subject, String description, Users creator) {
        this.adminOnly = false;
        this.subject = subject;
        this.description = description;
        this.creator = creator;
    }
    
    public boolean adminOnly(){
        return adminOnly;
    }
    public void setAdminOnly(boolean t){
        adminOnly = t;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getSubject() {
        return subject;
    }

    /**
     *
     * @param subject
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     */
    public List<Admins> getAdmins() {
        return admins;
    }

    /**
     *
     * @param admins
     */
    public void setAdminIDs(List<Admins> admins) {
        this.admins = admins;
    }

    /**
     *
     * @return
     */
    public Users getCreator() {
        return creator;
    }

    /**
     *
     * @param creator
     */
    public void setCreator(Users creator) {
        this.creator = creator;
    }

    /**
     *
     * @return
     */
    public List<ThreadComment> getComments() {
        return comments;
    }

    /**
     *
     * @param comments
     */
    public void setComments(List<ThreadComment> comments) {
        this.comments = comments;
    }

    /**
     *
     * @param comment
     */
    public void addComment(ThreadComment comment) {
        if (this.comments.isEmpty()) {
            comments.add(comment);
        } else if (!this.comments.contains(comment)) {
            comments.add(comment);
        }
    }

    /**
     *
     * @param admin
     */
    public void addAdmin(Admins admin) {
        if (this.admins.isEmpty()) {
            admins.add(admin);
        } else if (!this.admins.contains(admin)) {
            admins.add(admin);
        }

    }

}
