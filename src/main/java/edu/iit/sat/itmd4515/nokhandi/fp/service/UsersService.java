/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.fp.service;

//import edu.iit.sat.itmd4515.nokhandi.mp4.domain.security.SecureUser;
import edu.iit.sat.itmd4515.nokhandi.fp.domain.Users;
import edu.iit.sat.itmd4515.nokhandi.fp.domain.security.SecureUser;
import edu.iit.sat.itmd4515.nokhandi.fp.domain.security.SecureUserGroups;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Neil Okhandiar
 */
@Stateless
public class UsersService extends AbstractService<Users>{
 
    @EJB
    SecureUserService sUS;
    
    @EJB
    SecureUserGroupService sUGS;
    
    public UsersService() {
        super(Users.class);
    }

    /**
     *
     * @return
     */
    @Override
    public List<Users> findAll() {
        return getEntityManager().createNamedQuery("Users.findAll",Users.class).getResultList();
    }
    
    public Users find(String username){
        return getEntityManager().createNamedQuery("Users.findByUsername",
                Users.class)
                .setParameter("username", username)
                .getSingleResult();
    }
    
    public Users addUser(String username, String password){
        Users a = new Users(username, password);
        a.setUsername(username);
        a.setPassword(password);
        
        SecureUser b = new SecureUser(username, password);
        b.setUsername(username);
        b.setPassword(password);
        SecureUserGroups g = sUGS.findUsers();
        
        
        a.setUser(b);
        b.addGroup(g);
        g.addUser(b);
        
        sUS.create(b);
        sUGS.update(g);
        create(a);
        return a;
    }
    
    public Users updateUser(String username, String password){
        Users a = find(username);
        a.setPassword(password);
        update(a);
        return a;
    }
}
