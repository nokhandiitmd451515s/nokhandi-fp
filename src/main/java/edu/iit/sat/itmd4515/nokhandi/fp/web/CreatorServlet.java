/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.fp.web;

import edu.iit.sat.itmd4515.nokhandi.fp.domain.ForumThread;
import edu.iit.sat.itmd4515.nokhandi.fp.domain.ThreadComment;
import edu.iit.sat.itmd4515.nokhandi.fp.domain.Users;
import edu.iit.sat.itmd4515.nokhandi.fp.service.AdminService;
import edu.iit.sat.itmd4515.nokhandi.fp.service.SecureUserService;
import edu.iit.sat.itmd4515.nokhandi.fp.service.ThreadCommentService;
import edu.iit.sat.itmd4515.nokhandi.fp.service.ThreadService;
import edu.iit.sat.itmd4515.nokhandi.fp.service.UsersService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Neil Okhandiar
 */
//this webservlet will handle all processing for creating new items
//newComment: adds a new comment to the given thread
//newThread: adds a new thread
//newUser: adds a new user
//newAdmin: adds a new admin
@WebServlet(name = "creatorServlet", urlPatterns = {"/creatorServlet", "/newComment", "/newThread", "/newUser1", "/newUser2"})
public class CreatorServlet extends HttpServlet {

    @EJB
    private SecureUserService adminService;
    @EJB
    private AdminService adminService2;
    @EJB
    private UsersService usersService;
    @EJB
    private ThreadService threadService;
    @EJB
    private ThreadCommentService commentService;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            String path = request.getServletPath();
            //generates a new comment for a thread
            if (path.equals("/newComment")) {
                int threadId = Integer.parseInt(request.getParameter("threadId"));
                String username = request.getParameter("username");
                String content = request.getParameter("content");

                ForumThread thread = threadService.find(threadId);
                Users user = usersService.find(username);

                ThreadComment tC = commentService.newComment(thread, user, content);
                threadService.addComment(thread, tC);
                threadService.update(thread);

                String returnTo = "/nokhandi-fp/thread?param=" + threadId;
                response.sendRedirect(returnTo);
                
            } else if (path.equals("/newThread")) {
                String username = request.getParameter("username");
                String subject = request.getParameter("subject");
                String description = request.getParameter("description");

                Users user = usersService.find(username);

                threadService.newThread(subject, description, user);

                String returnTo = "/nokhandi-fp/allthreads";
                response.sendRedirect(returnTo);
                
            } else if (path.equals("/newUser1")) {
                String returnTo = "/newUser.jsp";
                getServletConfig().getServletContext().getRequestDispatcher(returnTo).forward(request, response);
                
            } else if (path.equals("/newUser2")) {
                String username = request.getParameter("username");
                String password = request.getParameter("password");

                usersService.addUser(username, password);
                String returnTo = "/nokhandi-fp/AdminTest";
                response.sendRedirect(returnTo);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
