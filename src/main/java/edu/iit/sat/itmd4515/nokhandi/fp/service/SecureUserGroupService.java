/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.fp.service;

import edu.iit.sat.itmd4515.nokhandi.fp.domain.security.SecureUserGroups;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author Neil Okhandiar
 */
@Stateless
public class SecureUserGroupService extends AbstractService<SecureUserGroups> {
   
    public SecureUserGroupService(){
        super(SecureUserGroups.class);
    }
    
    public SecureUserGroups findUsers(){
        return getEntityManager().createNamedQuery("SecureUserGroups.find",
                SecureUserGroups.class)
                .setParameter("groupname", "users")
                .getSingleResult();
    }
    public SecureUserGroups findAdmins(){
        return getEntityManager().createNamedQuery("SecureUserGroups.find",
                SecureUserGroups.class)
                .setParameter("groupname", "admins")
                .getSingleResult();
    }
    
    @Override
    public List<SecureUserGroups> findAll(){
        return getEntityManager().createNamedQuery("SecureUserGroups.findAll",
                SecureUserGroups.class).getResultList();
    }
    
}
