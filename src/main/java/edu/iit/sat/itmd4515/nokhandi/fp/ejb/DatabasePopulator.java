/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.fp.ejb;

import edu.iit.sat.itmd4515.nokhandi.fp.domain.Admins;
import edu.iit.sat.itmd4515.nokhandi.fp.domain.ThreadComment;
import edu.iit.sat.itmd4515.nokhandi.fp.domain.ForumThread;
import edu.iit.sat.itmd4515.nokhandi.fp.domain.Users;
import edu.iit.sat.itmd4515.nokhandi.fp.domain.security.SecureUser;
import edu.iit.sat.itmd4515.nokhandi.fp.domain.security.SecureUserGroups;
import edu.iit.sat.itmd4515.nokhandi.fp.service.AdminService;
import edu.iit.sat.itmd4515.nokhandi.fp.service.SecureUserService;
import edu.iit.sat.itmd4515.nokhandi.fp.service.ThreadCommentService;
import edu.iit.sat.itmd4515.nokhandi.fp.service.ThreadService;
import edu.iit.sat.itmd4515.nokhandi.fp.service.UsersService;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Neil Okhandiar
 */
@Startup
@Singleton
public class DatabasePopulator {

    @EJB
    private UsersService userService;

    @EJB
    private ThreadService threadService;

    @EJB
    private AdminService adminService;

    @EJB
    private ThreadCommentService threadCommentService;

    @EJB
    private SecureUserService secureUserService;

    @PersistenceContext(unitName = "nokhandiPU")
    private EntityManager em;

    public DatabasePopulator() {

    }

    //Just adds some sample data to the DB, to test with.
    @PostConstruct
    private void seedDatabase() {

        SecureUser neila = new SecureUser("neil", "itmd4515");
        SecureUser priyankaa = new SecureUser("priyanka", "itmd4515");
        SecureUser administrator = new SecureUser("administrator", "itmd4515");

        SecureUserGroups adminis = new SecureUserGroups("ADMINS", "a");
        SecureUserGroups useris = new SecureUserGroups("USERS", "b");
        neila.addGroup(useris);
        priyankaa.addGroup(useris);
        administrator.addGroup(adminis);

        Users neil = new Users("neil", "itmd4515");
        neil.setUsername("neil");
        neil.setPassword("itmd4515");
        neil.setUser(neila);

        Users priyanka = new Users("priyanka", "itmd4515");
        priyanka.setUsername("priyanka");
        priyanka.setPassword("itmd4515");
        priyanka.setUser(priyankaa);

        Admins adminNeil = new Admins();
        adminNeil.setUsername("administrator");
        adminNeil.setPassword("itmd4515");
        adminNeil.setUser(administrator);

        ForumThread thread1 = new ForumThread();
        thread1.setSubject("I wish I were not");
        thread1.setDescription("and were I not, I would be");
        thread1.setCreator(neil);

        ThreadComment tC11 = new ThreadComment();
        tC11.setThread(thread1);
        tC11.setContent("Thats stupid you loser");
        tC11.setUser(priyanka);

        ThreadComment tC12 = new ThreadComment();
        tC12.setThread(thread1);
        tC12.setContent("fuck you priyanka");
        tC12.setUser(neil);

        ForumThread thread2 = new ForumThread("I'm starting a thread", "I'm starting a description", priyanka);
        ThreadComment tC21 = new ThreadComment(thread2, neil, "why priyanka why");
        tC21.setContent("why priyanka why");
        ThreadComment tC22 = new ThreadComment(thread2, priyanka, "hey shut up");
        tC22.setContent("hey shut up");
        ThreadComment tC23 = new ThreadComment(thread2, neil, "why can't I just have someone worth talking to");
        tC23.setContent("why can't I just have someone worth talking to");
        ThreadComment tC24 = new ThreadComment(thread2, priyanka, "i will break your face you little wanker");
        tC24.setContent("i will break your face you little wanker");
        
        ForumThread adminThread = new ForumThread("An Admin Thread", "Only for admins!", adminNeil);
        ThreadComment tC31 = new ThreadComment(adminThread, adminNeil, "a comment!");
        adminThread.setAdminOnly(true);
        threadService.addComment(adminThread, tC31);
        
        List<ThreadComment> a = new ArrayList<>();
        a.add(tC11);
        a.add(tC12);
        thread1.setComments(a);

        List<ThreadComment> b = new ArrayList<>();
        b.add(tC21);
        b.add(tC22);
        b.add(tC23);
        b.add(tC24);
        thread2.setComments(b);
        
        
//        thread.addComment(tC11);
//        thread.addComment(tC12);
        
        
        threadCommentService.create(tC11);
        threadCommentService.create(tC12);
        threadCommentService.create(tC21);
        threadCommentService.create(tC22);
        threadCommentService.create(tC23);
        threadCommentService.create(tC24);
        threadCommentService.create(tC31);
        
        threadService.create(adminThread);
        threadService.create(thread1);
        threadService.create(thread2);

        em.persist(adminis);
        em.persist(useris);
        secureUserService.create(neila);
        secureUserService.create(priyankaa);
        secureUserService.create(administrator);
        userService.create(neil);
        userService.create(priyanka);
        adminService.create(adminNeil);
        System.out.println("Created neil itmd4515");
    }
}
