/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.fp.domain;

//import edu.iit.sat.itmd4515.nokhandi.mp4.domain.security.SecureUser;
import java.io.Serializable;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author nokhandiar
 * Comments appended to the forum thread
 * Each comment knows:
 *  The thread it belongs to
 *  The user who created it
 *  The time it was created
 * 
 */
@Entity

public class ThreadComment implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private Users user;
    private String content;

    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    //automagically sets timestamp on comment save
    @PrePersist
    private void setTimeStamp(){
        timestamp = GregorianCalendar.getInstance().getTime();
    }
    
    @ManyToOne
    private ForumThread thread;

    public ThreadComment() {
        
    }
    
    public ThreadComment(ForumThread thread, Users user, String Content){
        this.thread = thread;
        this.user = user;
        setContent(content);
//        this.content = content;
    }
    
    /**
     * simply gets the id of this thread comment
     * @return 
     */
    public Long getId() {
        return id;
    }

    /**
     * gets the timestamp for this comment
     * @return
     */
    public Date getTimeStamp(){
        return timestamp;
    }

    /**
     * gets the posting user's id
     * @return
     */
    public Users getUser() {
        return user;
    }

    /**
     * gets the content of the comment
     * @return
     */
    public String getContent() {
        return content;
    }

    /**
     * gets the owning thread
     * @return
     */
    public ForumThread getThread() {
        return thread;
    }

    /**
     * sets the id of the posting user
     * @param userID
     */
    public void setUser(Users user) {
        this.user = user;
    }

    /**
     * sets the content for the comment
     * @param content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * sets the owning thread
     * @param thread
     */
    public void setThread(ForumThread thread) {
        this.thread = thread;
    }

    
}
