/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.fp.domain.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Neil Okhandiar This bean is simply used to store the various roles a
 * user might fulfill Each user can currently be of USERS or ADMINS.
 */
@Entity
@Table(name = "sec_group")
@NamedQueries({
    @NamedQuery(name = "SecureUserGroups.findAll",
            query = "select u from SecureUserGroups u"),
    @NamedQuery(name = "SecureUserGroups.find",
            query = "select u from SecureUserGroups u where u.groupName = :groupname")})
public class SecureUserGroups implements Serializable {

    @Id
    private String groupName;
    private String groupDesc;

    @ManyToMany(mappedBy = "groups")
    private List<SecureUser> user = new ArrayList<>();

    public SecureUserGroups() {
    }

    public SecureUserGroups(String groupName, String groupDesc) {
        this.groupName = groupName;
        this.groupDesc = groupDesc;
    }

    public void addUser(SecureUser u) {
        if (!this.user.contains(u)) {
            this.user.add(u);
        }
        if (!u.getGroups().contains(this)) {
            u.getGroups().add(this);
        }
    }

    /**
     * Get the value of groupName
     *
     * @return the value of groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * Set the value of groupName
     *
     * @param groupName new value of groupName
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * Get the value of groupDesc
     *
     * @return the value of groupDesc
     */
    public String getGroupDesc() {
        return groupDesc;
    }

    /**
     * Set the value of groupDesc
     *
     * @param groupDesc new value of groupDesc
     */
    public void setGroupDesc(String groupDesc) {
        this.groupDesc = groupDesc;
    }

    public List<SecureUser> getUser() {
        return user;
    }

    public void setUser(List<SecureUser> users) {
        this.user = users;
    }
}
