/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.fp.service;

import edu.iit.sat.itmd4515.nokhandi.fp.domain.security.SecureUser;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author Neil Okhandiar
 */
@Stateless
public class SecureUserService extends AbstractService<SecureUser>{
    
    public SecureUserService() {
        super(SecureUser.class);
    }

    /**
     *
     * @return
     */
    @Override
    public List<SecureUser> findAll() {
        return getEntityManager().createNamedQuery("SecureUser.findAll",SecureUser.class).getResultList();
    }
    
    
    public SecureUser find(int id){
        return getEntityManager().createNamedQuery("SecureUser.findById",
                SecureUser.class)
                .setParameter("id", id)
                .getSingleResult();
    }
    
    public SecureUser find(String username){
        return getEntityManager().createNamedQuery("SecureUser.findByUsername",
                SecureUser.class)
                .setParameter("username", username)
                .getSingleResult();
    }
}
