/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.fp.web;

import edu.iit.sat.itmd4515.nokhandi.fp.domain.Admins;
import edu.iit.sat.itmd4515.nokhandi.fp.domain.ThreadComment;
import edu.iit.sat.itmd4515.nokhandi.fp.domain.Users;
import edu.iit.sat.itmd4515.nokhandi.fp.domain.ForumThread;
import edu.iit.sat.itmd4515.nokhandi.fp.service.AdminService;
import edu.iit.sat.itmd4515.nokhandi.fp.service.SecureUserService;
import edu.iit.sat.itmd4515.nokhandi.fp.service.ThreadService;
import edu.iit.sat.itmd4515.nokhandi.fp.service.UsersService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nokhandi
 */
@WebServlet(name = "RoleTestServlet", urlPatterns = {"/AdminTest", "/allthreads", "/thread"})
public class AdminTestPortal extends HttpServlet {

    @EJB
    private SecureUserService adminService;
    @EJB
    private AdminService adminService2;
    @EJB
    private UsersService usersService;
    @EJB
    private ThreadService threadService;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     *
     * Page simply states what role you're in, your username and your (hashed)
     * password
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        if (request.getServletPath().equals("/allthreads")) {
            List<ForumThread> threads = threadService.findAll();
            request.setAttribute("threads", threads);
            request.setAttribute("isAdmin", request.isUserInRole("Admins"));
            getServletConfig().getServletContext().getRequestDispatcher("/allThreads.jsp").forward(request, response);
            
        } else if (request.getServletPath().equals("/thread")) {
            int id = Integer.parseInt(request.getParameter("param"));
            request.setAttribute("thread", threadService.find(id));
            getServletConfig().getServletContext().getRequestDispatcher("/thread.jsp").forward(request, response);
        }

        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RoleTestServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RoleTestServlet at " + request.getContextPath() + "</h1>");

            request.authenticate(response);

            if (request.isUserInRole("Admins")) {
                Admins l = adminService2.find(request.getRemoteUser());
                out.println("<h2>" + l.getUsername() + " " + l.getPassword() + "</h2>");
                out.println("<h3> You've logged in as an Admin! </h3>");
            } else if (request.isUserInRole("USERS") || request.isUserInRole("Users")) {
                Users l = usersService.find(request.getRemoteUser());
                out.println("<h2>" + l.getUsername() + " " + l.getPassword() + "</h2>");
                out.println("<h3> You've logged in as an User! </h3>");
            } else {
                out.println("You are not logged in as either User or Admin <br> <h1> WHY ARE YOU HERE </h1>");
            }

            out.println("<a href=\"" + request.getContextPath() + "/logout\">Logout</a>");
            out.println("<br>");
            out.println("<a href=\"" + request.getContextPath() + "/allthreads\">See All Threads</a>");

            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
