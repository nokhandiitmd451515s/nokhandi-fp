/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.fp.service;

import edu.iit.sat.itmd4515.nokhandi.fp.domain.ForumThread;
import edu.iit.sat.itmd4515.nokhandi.fp.domain.ThreadComment;
import edu.iit.sat.itmd4515.nokhandi.fp.domain.Users;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author Neil Okhandiar
 */
@Stateless
public class ThreadCommentService extends AbstractService {

    public ThreadCommentService() {
        super(ThreadComment.class);
    }

    @Override
    public List<ThreadComment> findAll() {
        return getEntityManager().createNamedQuery("ThreadComment.findAll", ThreadComment.class).getResultList();
    }

    public ThreadComment newComment(ForumThread thread, Users user, String content) {
        ThreadComment tC = new ThreadComment();
        tC.setUser(user);
        tC.setThread(thread);
        tC.setContent(content);
        create(tC);
        return tC;
    }

}
