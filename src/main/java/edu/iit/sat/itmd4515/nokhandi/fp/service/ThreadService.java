/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.nokhandi.fp.service;

import edu.iit.sat.itmd4515.nokhandi.fp.domain.ForumThread;
import edu.iit.sat.itmd4515.nokhandi.fp.domain.ThreadComment;
import edu.iit.sat.itmd4515.nokhandi.fp.domain.Users;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author nokhandiar
 */
@Stateless
public class ThreadService extends AbstractService<ForumThread> {

    /**
     *
     */
    public ThreadService() {
        super(ForumThread.class);
    }

    /**
     *
     * @return
     */
    @Override
    public List<ForumThread> findAll() {
        return getEntityManager().createNamedQuery("ForumThread.findAll", ForumThread.class).getResultList();
    }

    public ForumThread find(int id) {
        return getEntityManager().createNamedQuery("ForumThread.findById",
                ForumThread.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public List<ThreadComment> findAllComments(ForumThread thread) {
        return thread.getComments();
    }

    public void addComment(ForumThread thread, ThreadComment tC) {
        List<ThreadComment> a = thread.getComments();
        if(a == null){
            a = new ArrayList<>();
        }
        a.add(tC);
        thread.setComments(a);
        update(thread);
    }
    
    public void newThread(String subject, String description, Users user){
        ForumThread thread = new ForumThread(subject, description, user);
        create(thread);
    }
}
