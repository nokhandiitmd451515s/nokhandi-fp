<%-- 
    Document   : allThreads
    Created on : May 7, 2015, 9:14:51 PM
    Author     : Neil Okhandiar
--%>

<%@page import="java.util.Vector"%>
<%@page import="edu.iit.sat.itmd4515.nokhandi.fp.domain.ForumThread"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">    

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/jumbotron-narrow.css" rel="stylesheet">
    </head>
    <body>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <h1>Threads !</h1>


        <c:forEach items="${threads}" var="t">
            <c:if test="${!t.adminOnly()}">
                <a href="
                   <c:out value = "${pageContext.servletContext.contextPath}"/>/thread?param=<c:out value = "${t.getId()}"/>"> 
                    <c:out value = "${t.getSubject()}"/> 
                </a>
                <br>
            </c:if>
        </c:forEach>

        <c:if test="${isAdmin}">
            <h3> Threads for Admins only ! </h3> 
            <br>
            <c:forEach items="${threads}" var="t">
                <c:if test="${t.adminOnly()}">
                    <a href="
                       <c:out value = "${pageContext.servletContext.contextPath}"/>/thread?param=<c:out value = "${t.getId()}"/>"> 
                        <c:out value = "${t.getSubject()}"/> 
                    </a>
                    <br>
                </c:if>
            </c:forEach>
        </c:if>

        <br>


        <form method="post" action="${pageContext.servletContext.contextPath}/newThread">
            <input type="hidden" name="username" value ="${pageContext.request.remoteUser}"/> <br>

            Subject :       <input required type="text"  name="subject"     /> <br>
            Description:    <input required type="text"  name="description" /> <br>

            <button type="submit"> Add a new Thread! </button>
        </form>
        <br>

        <a href="${pageContext.servletContext.contextPath}/logout">Logout</a>

    </body>
</html>
