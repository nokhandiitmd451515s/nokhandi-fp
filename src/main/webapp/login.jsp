<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">    

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/jumbotron-narrow.css" rel="stylesheet">
        <title>User Login Error JSP</title>
    </head>
    <body>
<!--        <h2>User Login</h2>
        <form method="post" action="j_security_check">
            <fieldset>
                <div>
                    <label for="j_username">Username:</label>
                    <input type="text" id="j_username" name="j_username" />
                </div>
                <div>
                    <label for="j_password">Password:</label>
                    <input type="password" id="j_password" name="j_password" />
                </div>
                <input type="submit" value="Submit"/>
                <input type="reset" value="Reset"/>
                <input type="submit" value="Add a new user !" formaction="/nokhandi-fp/newUser1">
            </fieldset>
        </form>-->

        <div class="container">
            <form class="form-signin" method="post" action="j_security_check">
                <h2 class="form-signin-heading">User Login</h2>

                <label for="j_username" class="sr-only">Username</label>
                <input type="text" id="j_username" name = "j_username" class="form-control" required="" autofocus="">

                <label for="j_password" class="sr-only">Password</label>
                <input type="password" id="j_password"  name="j_password" class="form-control" required="">

                <button class="btn btn-lg btn-primary btn-block" type="submit" value="Submit"> Submit </button>
                <button class="btn btn-lg btn-primary btn-block" type="reset" value="Reset"> Reset </button>
            </form>
            <br>
            <form class="form-signin">
            <button class="btn btn-lg btn-primary btn-block" type="submit" value="Add a new user !" formaction="/nokhandi-fp/newUser1"> Add a new user ! </button>
            </form>
        </div>
    </body>
</html> 