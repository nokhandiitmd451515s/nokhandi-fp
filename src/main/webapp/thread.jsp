<%-- 
    Document   : thread
    Created on : May 7, 2015, 11:10:22 PM
    Author     : Neil Okhandiar
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="edu.iit.sat.itmd4515.nokhandi.fp.domain.ThreadComment"%>
<%@page import="java.util.List"%>
<%@page import="edu.iit.sat.itmd4515.nokhandi.fp.domain.ForumThread"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">    

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/jumbotron-narrow.css" rel="stylesheet">
    </head>
    <body>

        <h2> ${thread.getSubject()} </h2>
        <h3> ${thread.getDescription()} </h3>
        <br>
        <br>
        <c:forEach var="c" items="${thread.getComments()}">
            <strong><font color ="red"> ${c.getContent()}  </font></strong>
            <br>
            <b> ${c.getUser().getUsername()} </b> ---- ${c.getTimeStamp()}
            <br>
            <br>
        </c:forEach>
        <br>
        <br>
        <br>
        <form method="post" action="${pageContext.servletContext.contextPath}/newComment">
            Content: <input required type="text" name="content" />
            <input type="hidden" name ="username" value= ${pageContext.request.remoteUser} />
            <input type="hidden" name="threadId" value="${thread.getId()}" />
            <button type="submit"> Add a comment ! </button>
        </form>

        <br>
        <a href="${pageContext.servletContext.contextPath}/allthreads"> back to all threads </a>

        <br>

        <a href="${pageContext.servletContext.contextPath}/logout">Logout</a>
    </body>
</html>
