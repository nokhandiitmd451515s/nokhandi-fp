<%-- 
    Document   : newUser
    Created on : May 8, 2015, 5:26:15 AM
    Author     : Neil Okhandiar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">    

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/jumbotron-narrow.css" rel="stylesheet">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>New User !</title>
    </head>
    <body>
        <form method="post" action="/nokhandi-fp/newUser2">

            Username:       <input required type="text"   name="username" /> <br>
            Password:       <input required type="password"   name="password" /> <br>

            <button type="submit"> Add a new User! </button>
        </form>
    </body>
</html>
